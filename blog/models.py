from django.db import models


class Category(models.Model):
	name = models.CharField(max_length=60)

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name_plural = 'Categories'

		
class Post(models.Model):
	title = models.CharField( max_length = 60)
	body = models.TextField()
	category = models.ForeignKey(Category)
	pub_date = models.DateTimeField(auto_now_add=True)
	published = models.BooleanField()

	def __unicode__(self):
		return self.title


