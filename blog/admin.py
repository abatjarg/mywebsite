from django.contrib import admin
from blog.models import Post, Category

class PostAdmin(admin.ModelAdmin):
	list_display = ('title','pub_date','category','published')
	list_filter = ['pub_date','category']
	search_fields = ['title']
	date_hierarchy = 'pub_date'

admin.site.register(Post,PostAdmin)

class CategoryAdmin(admin.ModelAdmin):
	search_fields = ['name']

admin.site.register(Category, CategoryAdmin)