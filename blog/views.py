from django.shortcuts import render
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from blog.models import Post, Category

def main(request):
	posts = Post.objects.filter(published=True).order_by('-pub_date')
	cats = Category.objects.all()
	paginator = Paginator(posts,2)

	try:
		page = int(request.GET.get('page','1'))
	except ValueError: page = 1

	try:
		posts = paginator.page(page)
	except (InvalidPage, EmptyPage):
		posts = paginator.page(paginator.num_pages)

	context = dict(posts=posts, cats = cats)

	return render(request,"blog.html", context)

def post(request, pk):
    post = Post.objects.get(pk=int(pk))
    context = dict(post=post, user=request.user)
    return render(request, "post.html", context)

def cat(request, pk):
	category = Category.objects.get(pk=int(pk))
	posts = Post.objects.filter(category = category).order_by('-pub_date')
	return render(request,"category.html", dict(category=category, posts=posts))
