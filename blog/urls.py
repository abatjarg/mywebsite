from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
     url(r'^$', 'blog.views.main', name="main"),
     url(r'^post/(?P<pk>\d+)/$', 'blog.views.post', name="post"),
     url(r'^category/(?P<pk>\d+)/$', 'blog.views.cat', name="category"),
)